/// <reference path='node.d.ts'/>
import http = require("http");
import fs = require("fs");
import url = require('url');
import marked = require('marked');
import querystring = require('querystring');

/**
 * Wiki ページをレンダリングします。
 * @param res リクエストを返す先
 * @param title Wiki ページのタイトル
 */
function render(res: http.ServerResponse, title: string): void {
    var path: string = 'pages/' + title + ".md";
    var exists: boolean = fs.existsSync(path);
    var markdown: string = exists ? fs.readFileSync(path, 'utf8') : '';    
    var output: string = fs.readFileSync('templete.html', 'utf8');    
    
    // テンプレート内の {{key}} という部分を、すべて正規表現で置換します
    var table: { [key:string]: string } = {
        "title": title,
        "pagetitle": title + (exists ? '' : '(ページの新規作成)'),
        "content": marked(markdown),
        "timestamp": exists ? fs.statSync(path).mtime.toString() : ' ---- ',
        "source": markdown
    };    
    Object.getOwnPropertyNames(table).map(function(key: string){
        output = output.replace(new RegExp('{{' + key + '}}', 'gi'), table[key]);
    });

    // HTTP ヘッダとメッセージボディを書き込みます
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(output);
}

// http サーバを作成します
var server: http.Server = http.createServer();

// リクエストがあった時のイベントハンドラを設定します
server.on('request', function(req: http.IncomingMessage, res: http.ServerResponse){
    // リクエストされた URL からパス名の部分だけを切り出します
    var pathname: string = url.parse(req.url).pathname;

    // リソース用のディレクトリを参照したとき
    var resoruce = /^\/res\/(.*)$/.exec(pathname);
    if(resoruce){
        var r = resoruce[1];
        // javascript がリクエストされたとき
        if(r.match(/^\w+\.js$/)){
            // リクエストされたファイルの内容を読み込んでそのまま返します
            res.writeHead(200, { 'Content-Type': 'text/javascript' });
            res.end(fs.readFileSync('.' + pathname, 'utf8'));
        }

        // css がリクエストされたとき
        else if(r.match(/^\w+\.css$/)){
            res.writeHead(200, { 'Content-Type': 'text/css' });
            res.end(fs.readFileSync('.' + pathname, 'utf8'));                
        }
    }

    // wiki ページがリクエストされたとき
    else if(pathname.match(/^\/\w+$/)){
        // パス名から Wiki ページのタイトルを抜き出します
        var title: string = /^\/(\w+)$/.exec(pathname)[1];

        // メソッドが POST の場合は Wiki　ページの保存を行います。
        if(req.method == 'POST'){
            var buffer: string = "";

            // ストリームから読み込み可能になると、readable イベントが発生します。
            req.on('readable', function(){
                // 読み込み可能なときは、read を呼び出すとストリームからデータを読み込みます。
                buffer += req.read().toString();
            });              

            // メッセージの終端まで読み込むと、end イベントが発生します。
            req.on('end', function(){
                try{
                    // クエリをパースしてテーブルにします
                    var query = querystring.parse(buffer);
                    
                    // クエリの source というフィールドに保存する Wiki ソーステキストが入っているので、ファイルに保存します
                    fs.writeFileSync('pages/' + title + '.md', query['source']);

                    console.log("page " + title + " updated");
                }catch(e){
                    // よくわからない壊れたクエリが送られてきた場合は、ログを残して無視します
                    console.log('malformed post data');
                }
                // 更新したページをクライアントに返します
                render(res, title);
            });             
        }

        // POST でなければ、そのままリクエストされたページをクライアントに返します
        else{
            render(res, title);
        }
    }

    // そのほかのよくわからないリクエストが来たときは、とりあえずトップページを表示します
    else{
        render(res, 'top');
    }
});

// サーバのリクエスト待機を開始します。
server.listen(80);
console.log('Nodense runninng!');