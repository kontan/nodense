var http = require("http");
var fs = require("fs");
var url = require('url');
var marked = require('marked');
var querystring = require('querystring');

function render(res, title) {
    var path = 'pages/' + title + ".md";
    var exists = fs.existsSync(path);
    var markdown = exists ? fs.readFileSync(path, 'utf8') : '';
    var output = fs.readFileSync('templete.html', 'utf8');

    var table = {
        "title": title,
        "pagetitle": title + (exists ? '' : '(ページの新規作成)'),
        "content": marked(markdown),
        "timestamp": exists ? fs.statSync(path).mtime.toString() : ' ---- ',
        "source": markdown
    };
    Object.getOwnPropertyNames(table).map(function (key) {
        output = output.replace(new RegExp('{{' + key + '}}', 'gi'), table[key]);
    });

    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(output);
}

var server = http.createServer();

server.on('request', function (req, res) {
    var pathname = url.parse(req.url).pathname;

    var resoruce = /^\/res\/(.*)$/.exec(pathname);
    if (resoruce) {
        var r = resoruce[1];

        if (r.match(/^\w+\.js$/)) {
            res.writeHead(200, { 'Content-Type': 'text/javascript' });
            res.end(fs.readFileSync('.' + pathname, 'utf8'));
        } else if (r.match(/^\w+\.css$/)) {
            res.writeHead(200, { 'Content-Type': 'text/css' });
            res.end(fs.readFileSync('.' + pathname, 'utf8'));
        }
    } else if (pathname.match(/^\/\w+$/)) {
        var title = /^\/(\w+)$/.exec(pathname)[1];

        if (req.method == 'POST') {
            var buffer = "";

            req.on('readable', function () {
                buffer += req.read().toString();
            });

            req.on('end', function () {
                try  {
                    var query = querystring.parse(buffer);

                    fs.writeFileSync('pages/' + title + '.md', query['source']);

                    console.log("page " + title + " updated");
                } catch (e) {
                    console.log('malformed post data');
                }

                render(res, title);
            });
        } else {
            render(res, title);
        }
    } else {
        render(res, 'top');
    }
});

server.listen(80);
console.log('Nodense runninng!');

