window.addEventListener('load', function () {
    var waiting = true;
    var content = document.querySelector('#content');
    var textarea = document.querySelector('textarea');
    var submit = document.querySelector('input[type="submit"]');
    var initialText = textarea.value;
    submit.disabled = true;

    textarea.addEventListener('keyup', function () {
        submit.disabled = initialText == textarea.value;
        if (waiting) {
            waiting = false;
            content.innerHTML = marked(textarea.value);
            setTimeout(function () {
                waiting = true;
            }, 100);
        }
    });
});
