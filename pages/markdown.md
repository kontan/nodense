[top](top) > markdown

\*\*\* アスタリスクが３つ以上で水平線↓
***

# \# で見出し1

## \#\# で見出し2。以下続く

\**アスタリスク一個ずつで囲むと強調1*\*

\*\***アスタリスク２個づつで囲むと強調2**\*\*


[ゆゆ式アニメ公式サイト](http://www.yuyushiki.net/index.html)

\[ゆゆ式アニメ公式サイト\]\(http://www.yuyushiki.net/index.html\) ってかくとリンクになる

ちなみにモノホンの Wiki みたいにサイト内リンクを簡単に書く方法は存在しないよ。この nodense 内の別のページにリンクするには、\[top\]\(/top\) みたいに書く必要があるよ！



\!\[キャプション\]\(url\) で画像も書けるよ

![yuzuko](http://www.yuyushiki.net/core_sys/images/main/cont/chara/chara_navi1h.gif)

コード:

    public static void main(){
        // コード部分は等幅フォント
    }