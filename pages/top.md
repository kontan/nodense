Nodense
====================

![Nodense](http://1.bp.blogspot.com/-DMtF247WCAM/Tq2hov2PPHI/AAAAAAAABHw/-4y2m6KGAXU/s200/Nodens.jpg)

**Nodense** は node.js 環境で動作する簡易 Wiki エンジンです。このページはそのデモサイトになります。テキストは [markdown](http://daringfireball.net/projects/markdown/) で記述する事が可能で、リアルタイムにプレビューしながら編集することができます。サーバ側のソースコードは 100 行くらいの TypeScript で書かれています。

**詳しくはコミックマーケット 84 で頒布予定のプログラミング同人誌 [bit-fragment Vol.3](http://bit.plasticheart.info/) を御覧ください。**

## 使い方

ページを閲覧するには、`/ページ名` という URL にアクセスします。ページの下方にテキストボックスがあり、そこに markdown ソーステキストを入力するとリアルタイムでプレビューしながら編集できます。

編集が完了したら、右上の『保存』ボタンを押すとページが保存されます。もしそのページがまだ存在しない場合は、そのままそのページを編集して保存すればそのページが新規作成されます。

### ページの新規作成

ページを新規作成するには、**/ページ名** という URL に移動します。そのページが存在しなくても同様の編集画面になるので、そのまま編集して保存すればページが新規作成されます。
 
## ☢注意☢

nodense は node.js 解説用のサンプルプログラムに過ぎないので、セキュリティについては何も考慮されていません。何かあっても作者は責任をおいかねます(´・ω・｀)

また版の管理はしないので、誤ってページの内容が失われても復旧はできません。

## そのほか

* [markdown 簡易リファレンス](markdown)
* [FAQ](faq)
* [ソースコード](https://bitbucket.org/kontan/nodense)
* [nodense で使っているものリスト](tools)

<style>
img{
    float: right;
    margin: 8px;
}
</style>

<script type="text/javascript">
console.log("hoge");
</script>