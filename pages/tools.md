< [top](/top)

## お世話になっている方々

* [node.js](http://nodejs.org/)
* [TypeScript](http://www.typescriptlang.org/)
* [markdown](http://daringfireball.net/projects/markdown/)
* [marked](https://github.com/chjj/marked)
* [Node Ninja](http://node-ninja.com/)